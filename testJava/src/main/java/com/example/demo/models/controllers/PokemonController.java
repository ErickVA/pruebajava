package com.example.demo.models.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.models.Pokemon;
import com.example.demo.models.service.IPokemonService;

/**
 * 
 * @author Erick
 * Controlador para consumir los servicios de tipo Rest
 */
@RestController
@RequestMapping("/api")
public class PokemonController {
	
	
	/**
	 * Inyeccion de dependencia de la interfaz IPokemonService
	 * @see IPokemonService
	 */
	@Autowired
	private IPokemonService pokemonService;
	
	/**
	 * 
	 *  @return retorna una lista con los elementos traidos desde una api externa al proyecto
         *    
	 */
	@GetMapping("/pokeapi1gn")
	public ResponseEntity<?> getPokemongen1(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=151&offset=1", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
      
        System.out.println("Pokemon de primera generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	
	/**
	 * 
	 *  @return retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	
	@GetMapping("/pokeapi2gn")
	public ResponseEntity<?> getPokemongen2(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=152&offset=251", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemon de segunda Generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	
	/**
	 * 
	 *  @return retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokeapi3gn")
	public ResponseEntity<?> getPokemongen3(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=252&offset=386", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemon de tercera Generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	/**
	 * 
	 *@return retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokeapi4gn")
	public ResponseEntity<?> getPokemongen4(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=387&offset=493", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemon de Cuarta Generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	/**
	 * 
	 *  @return retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokeapi5gn")
	public ResponseEntity<?> getPokemongen5(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=494&offset=649", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemon de Quinta Generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	/*
	 * 
	 *  retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokeapi6gn")
	public ResponseEntity<?> getPokemongen6(){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit=650&offset=721", String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemon de Sexta Generacion: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	/**
	 * 
	 * @param limit el tope maximo de pokemon
	 * @param desde el inicio desde que pokemon quiere la lista
	 * @return  retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokemonTo")
	public ResponseEntity<?> getPokemos(@RequestParam(name="hasta", required=true) int limit, @RequestParam(name="desde",required=true) int desde){
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon?limit="+limit+"&offset="+desde, String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemones: "+myList);
        
        //si no hay contenido en la lista o ningun parametro no coinicide se manda un mensaje de erro
        
        if(myList.isEmpty()) {
        	
        	return new ResponseEntity<>("La lista esta vacia",HttpStatus.NO_CONTENT);
        }
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param numero de pokemon para obtener toda su informacion
	 * @return  retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokemonBy/{numero}")
	public ResponseEntity<?> getPokemonBy(@PathVariable Long numero){
		
		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon/"+numero, String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
        System.out.println("Pokemones: "+myList);
        
//si no hay contenido en la lista o ningun parametro no coinicide se manda un mensaje de erro
        
        if(myList.isEmpty()) {
        	
        	return new ResponseEntity<>("La lista esta vacia",HttpStatus.NO_CONTENT);
        }
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	/**
	 * 
	 * @param nombre busca por el nombre del pokemon
	 * @return  retorna una lista con los elementos traidos desde una api externa al proyecto
	 */
	@GetMapping("/pokemonBy")
	public ResponseEntity<?>getPokemonBy(@RequestParam(name = "nombre") String nombre){
		

		RestTemplate plantilla = new RestTemplate();
        String resultado = plantilla.getForObject("https://pokeapi.co/api/v2/pokemon/"+nombre, String.class);
        
        List<String> myList = new ArrayList<String>(Arrays.asList(resultado.split(",")));
        
//si no hay contenido en la lista o ningun parametro no coinicide se manda un mensaje de erro
        
        if(myList.isEmpty()) {
        	
        	return new ResponseEntity<>("La lista esta vacia",HttpStatus.NO_CONTENT);
        }
        
        System.out.println("Pokemones: "+myList);
        
		return new ResponseEntity<>(myList,HttpStatus.OK);
	}
	
	/*
	 * 
	 *  retorna una lista con todos lo pokemones en la base de datos
	 */
	@GetMapping("/pokemones")
	public ResponseEntity<?> findAll(){
		
		Map<String,Object> response = new HashMap<>();
		List<Pokemon> p = null;
		
		try {
			p = pokemonService.getPokemons();
		} catch (DataAccessException e) {
			response.put("Mensaje","Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(p.isEmpty()) {
			response.put("Mensaje", "No hay elementos en la base de datos");
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		System.out.println(response);
		return new ResponseEntity<List<Pokemon>>(p,HttpStatus.OK);
	}
	
	/**
	 * 
	 * @param especie
         * la especie de poquemon para buscar
	 *  @return  retorna una lista con todos lo pokemones en la base de datos
	 */
	@GetMapping("/pokemon/{especie}")
	public ResponseEntity<?> findAllByEspecie(@PathVariable String especie){
		
		Map<String,Object> response = new HashMap<>();
		List<Pokemon> p = null;
		try {
			p = pokemonService.getPokemonsByEspecie(especie);
		}catch (DataAccessException e) {
			response.put("Mensaje","Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(p.isEmpty()) {
			response.put("Mensaje","El pokemon con Especie ".concat(especie).concat(" no existe en la base de datos."));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
			
		}
		
		System.out.println(response);
		return new ResponseEntity<List<Pokemon>>(p,HttpStatus.OK);  
	}
	
	/**
	 * 
	 * @param apodo el apodo por el cual se busca el pokemon
	 * @return  retorna una lista con todos lo pokemones en la base de datos
	 */
	@GetMapping("/pokemones/{apodo}")
	public ResponseEntity<?> findAllByApodo(@PathVariable String apodo){
		
		Map<String,Object> response = new HashMap<>();
		List<Pokemon> p = null;
		try {
			p = pokemonService.getPokemonsByApodo(apodo);
		}catch (DataAccessException e) {
			response.put("Mensaje","Error al realizar la consulta en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		if(p.isEmpty()) {
			response.put("Mensaje","El pokemon con Apodo ".concat(apodo).concat(" no existe en la base de datos."));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		System.out.println("Pokemon:"+p.toString());
		return new ResponseEntity<List<Pokemon>>(p,HttpStatus.OK);  
	}
	
	/**
	 * 
	 * @param p objeto de la clase pokemon
	 * @return Un objeto de la clase Pokemon
	 * @see Pokemon
	 */
	@PostMapping("/save")
	public ResponseEntity<?> crearPokemon(@RequestBody Pokemon p){
		
		Map<String,Object> response = new HashMap<>();
		Pokemon pokemon = null;
		
		try {
			//generar numero aleatorios
			p.setAtaque((int) Math.floor(Math.random()*15));
			p.setExperiencia((int) Math.floor(Math.random()*15));
			p.setDefensa((int) Math.floor(Math.random()*15));
			//registro del pokemon
			pokemon= pokemonService.createProkemon(p);
		} catch (DataAccessException e) {
			response.put("Mensaje","Error, no se pudo registrar en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			System.err.println(response);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		response.put("Mensaje", "El Pokemon ha sido creado con exito");
		response.put("Pokemon:", pokemon);
		
		System.out.println(response);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
		
	}
	@GetMapping("/help")
        public String help(){
            String html = "";
            
            html+= 
            "<!DOCTYPE html>\n" +
"<html>\n" +
"  <head>\n" +
"    <meta charset=\"utf-8\">\n" +
"    <title>Pagina de Ayuda</title>\n" +
"  </head>\n" +
"  <body>"
                    + "<ul>"
                    + "<li>Para consumir el punto A:</li>"
                    + "<li>Usar estos Verbos: Get, con las siguientes rutas:</li>"
                    + "</ul> <br>"
                    + "<p>Para Primera Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi1gn -> Se espera status HTTP 200</p>"
                    + "<p>Para Segunda Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi2gn -> Se espera status HTTP 200</p>"
                    + "<p>Para Tercera Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi3gn -> Se espera status HTTP 200</p>"
                    + "<p>Para Cuarta Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi4gn -> Se espera status HTTP 200</p>"
                    + "<p>Para Quinta Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi5gn -> Se espera status HTTP 200</p>"
                    + "<p>Para Sexta Generacion:</p>"
                    + "<p>http://localhost:8080/api/pokeapi6gn -> Se espera status HTTP 200</p>"
                    + "<ul>"
                    + "<li>Para el punto B:</li>"
                    + "<li>Consumir en la siguiente url de la siguiente manera http://localhost:8080/api/pokemonTo?hasta=100&desde=20 -> se espera un"
                    + "http status 200 si todo sale bine y si no saldra un 404, esto quiere decir que el recurso solicitado no fue encontrado"
                    + "</li>"
                    + "<li>Para el punto C:</li>"
                    + "<li>Consumir en la siguientes urls: "
                    + "http://localhost:8080/api/pokemonBy?nombre=ditto en este caso es por nombre -> se espera un"
                    + "http status 200 si todo sale bine y si no saldra un 404, esto quiere decir que el recurso solicitado no fue encontrado</li>"
                    + "<li>http://localhost:8080/api/pokemonBy/2 en este caso es por numero de pokemon -> se espera un"
                    + "http status 200 si todo sale bine y si no saldra un 404, esto quiere decir que el recurso solicitado no fue encontrado</li>"
                    + "<li>Para el punto D:</li>"
                    + "</ul>"
                    + "<p>Para guardar un pokemon en la base de datos usar la siguiente url: http://localhost:8080/api/save -> se espera un"
                    + "http status 201  si todo sale bien y se creo algo y si no saldra un 500, esto quiere decir que hubo un problema con el backend</p>"
                    + "En postman Utilizar vero POST y el siguiente JSON"
                    + "'{\"apodo\": \"Daggeron\",\"especie\": \"Fuego\"}'"
                    + "<ul>"
                    + "<li>Para el putno E: Usar verbos GET</li>"
                    + "<li>Consumir esta url http://localhost:8080/api/pokemones/{apodo} -> en donde estan las llaves cambiar el apodo que desea buscar -> ej http://localhost:8080/api/pokemones/Erick"
                    + "se espera esattus 200 si todo sale bien y un 204 si todo salio bien pero el recurso esta vacio</li>"
                    + "<li>Para el recurso F: Usar verbos GET</li>"
                    + "<li>Consumir esta url http://localhost:8080/api/pokemon/{especie} -> en donde estan las llaves cambiar la especie que desea buscar -> ej http://localhost:8080/api/pokemon/Tierra"
                    + "se espera esattus 200 si todo sale bien y un 204 si todo salio bien pero el recurso esta vacio</li>"
                    + "</ul>"
                    + ""
                    + "<h3>Datos de contacto</h3>"
                    + "Erick Omar Velazquez"
                    + "Telefono: 5523684575"
                    + "email:erick.omardev@gmail.com" +
                    
                  
"  </body>" +
"</html>";
            
            
            return html;
        }

}
