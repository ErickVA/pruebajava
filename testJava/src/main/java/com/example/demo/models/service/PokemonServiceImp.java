package com.example.demo.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.Pokemon;
import com.example.demo.models.dao.IPokemon;

/**
 * 
 * @author Erick
 * Clase de servico que hace una instancia con la interfaz para ejecutar los metodos
 *
 */
@Service
public class PokemonServiceImp implements IPokemonService{

	/**
	 * inyeccion de dependencia
	 * @see IPokemon que ejecuta los metodos de CrudRepository
	 */
	@Autowired
	private IPokemon pokemonDao;
	
	
	@Override
	/**
	 * @return retorna ua lista con los elementos
	 */
	public List<Pokemon> getPokemons() {
		// TODO Auto-generated method stub
		return (List<Pokemon>) pokemonDao.findAll();
	}

	@Override
	/**
	 * @param especie la especie del poekmon a buscar
	 * 
	 * @return  retorna una lista de elementos ordenados por la especie
	 */
	public List<Pokemon> getPokemonsByEspecie(String especie) {
		// TODO Auto-generated method stub
		return pokemonDao.findAllByEspecie(especie);
	}

	@Override
	/**
	 * @param p objeto de la clase pokemon
	 * 
	 * return Pokemon retorna el objeto pokemon
	 */
	public Pokemon createProkemon(Pokemon p) {
		// TODO Auto-generated method stub
		return pokemonDao.save(p);
	}

	@Override
	/**
	 * @param apodo el apsodo con el que se busca el pokemon
	 * 
	 * return List<Pokemon> retorna una lista de elementos ordenados por el apodo
	 */
	public List<Pokemon> getPokemonsByApodo(String apodo) {
		// TODO Auto-generated method stub
		return pokemonDao.findAllByApodo(apodo);
	}

	

}
