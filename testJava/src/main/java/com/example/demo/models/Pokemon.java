package com.example.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @author Erick omar
 *
 */
@Entity
@Table(name = "pokemons")
public class Pokemon {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	//id que tendra el pokemon en la base de datos
	private Long id;
	
	
	@Column(unique = true)
	//apodo que tendra el poquemon en la base de datos, en este caso sera unico y no se podra repetir
	private String apodo;
	
	@Column
	//especie del pokemon
	private String especie;
	
	@Column
	//ataque del pokemon de 0-15
	private Integer ataque;
	@Column
	//defensa del pokemon de 0-15
	private Integer defensa;
	@Column	
	//xp del pokemon de 0-15
	private Integer experiencia;
	
	
	
	public Pokemon(Long id, String apodo, String especie, Integer ataque, Integer defensa, Integer experiencia) {
		super();
		this.id = id;
		this.apodo = apodo;
		this.especie = especie;
		this.ataque = ataque;
		this.defensa = defensa;
		this.experiencia = experiencia;
	}
	
	
	
	public Pokemon() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getApodo() {
		return apodo;
	}
	public void setApodo(String apodo) {
		this.apodo = apodo;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public Integer getAtaque() {
		return ataque;
	}
	public void setAtaque(Integer ataque) {
		this.ataque = ataque;
	}
	public Integer getDefensa() {
		return defensa;
	}
	public void setDefensa(Integer defensa) {
		this.defensa = defensa;
	}
	public Integer getExperiencia() {
		return experiencia;
	}
	public void setExperiencia(Integer experiencia) {
		this.experiencia = experiencia;
	}



	@Override
	public String toString() {
		return "Pokemon [id=" + id + ", apodo=" + apodo + ", especie=" + especie + ", ataque=" + ataque + ", defensa="
				+ defensa + ", experiencia=" + experiencia + "]";
	}	
	
	

}
