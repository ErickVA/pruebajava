package com.example.demo.models.service;

import java.util.List;

import com.example.demo.models.Pokemon;

/**
 * 
 * @author Erick
 *	Interfaz para la clase de servico
 */
public interface IPokemonService {

	/**
	 * Metodo que nos permite listar todos los elementos
	 * @return retorna una lista con todos los Pokemones
	 */
	public List<Pokemon> getPokemons();
	/**
	 * @param especie
	 * @return  retorna una lista con los pokemones de
	 * acuerdo al criterio de busqueda
	 */
	public List<Pokemon> getPokemonsByEspecie(String especie);
	/**
	 * 
	 * @param apodo
	 * @return  retorna una lista con los pokemones de
	 * acuerdo al criterio de busqueda
	 */
	public List<Pokemon> getPokemonsByApodo(String apodo);
	/**
	 * 
	 * @param p Objetico de la clase Pokemon
	 * @return regresa un objeto de la clase pokemon
	 */
	public Pokemon createProkemon(Pokemon p);
}
