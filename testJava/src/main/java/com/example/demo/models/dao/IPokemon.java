package com.example.demo.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.Pokemon;
/**
 * 
 * @author Erick
 * Interfaz para las operacion de base de datos (CRUD)
 */
public interface IPokemon extends CrudRepository<Pokemon, Long>{
	
	/**
	 * @param especie especie para el pokemon
	 * @return  retorna una lista con los pokemones de
	 * acuerdo al criterio de busqueda
	 */
	@Query("select p from Pokemon p where p.especie = ?1")
	public List<Pokemon> findAllByEspecie(String especie);
	
	/**
	 * 
	 * @param apodo apodo para el pokemon
	 * @return  retorna una lista con los pokemones de
	 * acuerdo al criterio de busqueda
	 */
	@Query("select p from Pokemon p where p.apodo = ?1")
	public List<Pokemon> findAllByApodo(String apodo);

}
